const bcrypt = require('bcrypt');
const asyncHandler = require('../middlewares/async');
const ErrorResponse = require('../utils/errorResponse');
const jwt = require('../utils/jwt');
const sendgrid = require('../utils/sendgrid');
const Contributor = require('../models/Contributor');

// @desc      Login
// @route     POST /api/admin/login
// @route     POST /api/contributor/login
// @route     POST /api/user/login
// @access    Public
exports.login = (model) =>
  asyncHandler(async (req, res, next) => {
    const { username, password } = req.body;

    const resource = await model
      .findOne({
        username,
      })
      .select('+password');

    if (model === Contributor && resource && !resource['isApproved']) {
      return next(
        new ErrorResponse('Contributor account not yet approved', 401)
      );
    }

    if (resource && (await resource.matchPassword(password))) {
      jwt.createToken({ userId: resource['_id'] }).then((token) => {
        res.status(200).json({
          success: true,
          token,
          username
        });
      });
    } else {
      return next(new ErrorResponse('Invalid credentials', 400));
    }
  });

// @desc      Signup
// @route     POST /api/contributor/signup
// @route     POST /api/user/signup
// @access    Public
exports.signup = (model) =>
  asyncHandler(async (req, res, next) => {
    const { username, email } = req.body;

    if (await model.findOne({ username })) {
      return next(new ErrorResponse('Username taken', 400));
    } else if (await model.findOne({ email })) {
      return next(
        new ErrorResponse('User with this email already exists', 400)
      );
    } else {
      const resource = await model.create(req.body);
      jwt.createToken({ userId: resource['_id'] }).then((token) => {
        res.status(201).json({
          status: true,
          token,
        });
      });
    }
  });

// @desc      Get current user
// @route     POST /api/contributor/current
// @route     POST /api/user/current
// @access    Private
exports.getCurrentUser = (model) =>
  asyncHandler(async (req, res, next) => {
    const { userId } = req.user;

    const resource = await model.findById(userId);

    res.status(201).json({
      status: true,
      data: resource,
    });
  });

// @desc      Send reset link
// @route     POST /api/contributor/send-reset-link
// @route     POST /api/user/send-reset-link
// @access    Public
exports.sendResetLink = (model) =>
  asyncHandler(async (req, res, next) => {
    const { email } = req.body;
    const resource = await model.findOne({ email });

    jwt.createToken({ userId: resource._id }).then((token) => {
      sendgrid
        .sendEmail(email, 'Reset password', `<p>${token}</p>`)
        .then(() => res.status(200).json({ success: true }));
    });
  });

// @desc      Send username
// @route     POST /api/contributor/send-username
// @route     POST /api/user/send-username
// @access    Public
exports.sendUsername = (model) =>
  asyncHandler(async (req, res, next) => {
    const { email } = req.body;
    const resource = await model.findOne({ email });

    sendgrid
      .sendEmail(
        email,
        'Your username',
        `<p>Username: ${resource.username}</p>`
      )
      .then(() => res.status(200).json({ success: true }));
  });

// @desc      Reset password
// @route     POST /api/contributor/reset-password
// @route     POST /api/user/reset-password
// @access    Public
exports.resetPassword = (model) =>
  asyncHandler(async (req, res, next) => {
    const { token, password } = req.body;
    jwt
      .verifyToken(token)
      .then(async (token) => {
        const passHash = await bcrypt.hash(password, 10);
        const resource = await model.findByIdAndUpdate(token.userId, {
          password: passHash,
        });
        res.status(200).json({
          success: true,
        });
      })
      .catch(next);
  });
