const { ObjectId } = require('mongodb');
const Admin = require('../models/Admin');
const Category = require('../models/Category');
const asyncHandler = require('../middlewares/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc      Get all categories
// @route     GET /api/category
// @access    Public
exports.getAllCategories = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc      Get single category
// @route     GET /api/category/:id
// @access    Public
exports.getCategory = asyncHandler(async (req, res, next) => {
  const category = await Category.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  }).populate({
    path: 'videos',
    select: '_id title thumbnail preview -categories',
    match: { isApproved: true, isDisabled: false },
  });

  if (!category) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  res.status(200).json({
    success: true,
    data: category,
  });
});

// @desc      Create category
// @route     POST /api/category
// @access    Private
exports.createCategory = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  const category = await Category.create(req.body);

  res.status(201).json({
    success: true,
    data: category,
  });
});

// @desc      Update category
// @route     PUT /api/v1/category/:id
// @access    Private
exports.updateCategory = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  let found = await Category.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  const updated = await Category.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: updated,
  });
});

// @desc      Delete category
// @route     DELETE /api/category/:id
// @access    Private
exports.deleteCategory = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  let found = await Category.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  const updated = await Category.findByIdAndUpdate(req.params.id, {
    isDisabled: true,
  });

  res.status(200).json({
    success: true,
  });
});
