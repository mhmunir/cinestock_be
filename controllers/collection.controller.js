const { ObjectId } = require('mongodb');
const Admin = require('../models/Admin');
const Contributor = require('../models/Contributor');
const Collection = require('../models/Collection');
const asyncHandler = require('../middlewares/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc      Get all collections
// @route     GET /api/collection
// @access    Public
exports.getAllCollections = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc      Get single collection
// @route     GET /api/collection/:id
// @access    Public
exports.getCollection = asyncHandler(async (req, res, next) => {
  const collection = await Collection.findOne({
    _id: req.params.id,
    isApproved: true,
    isDisabled: false,
  })
    .populate({
      path: 'contributor',
      select: '_id realName',
      match: { isApproved: true, isDisabled: false },
    })
    .populate({
      path: 'videos',
      select: '_id title thumbnail preview -collections duration resolution',
      match: { isApproved: true, isDisabled: false },
    });

  if (!collection) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  res.status(200).json({
    success: true,
    data: collection,
  });
});

// @desc      Create collection
// @route     POST /api/collection
// @access    Private
exports.createCollection = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) {
    const contributor = await Contributor.findOne({
      _id: ObjectId(userId),
      isApproved: true,
      isDisabled: false,
    });
    if (!contributor) return next(new ErrorResponse('Forbidden', 403));
    delete req.body['isApproved'];
    delete req.body['isFeatured'];
  }

  const collection = await Collection.create({
    ...req.body,
    contributor: admin ? null : userId,
    byCinestock: !!admin,
  });

  res.status(201).json({
    success: true,
    data: collection,
  });
});

// @desc      Update collection
// @route     PUT /api/v1/collection/:id
// @access    Private
exports.updateCollection = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) {
    const contributor = Contributor.findOne({
      _id: ObjectId(userId),
      isApproved: true,
      isDisabled: false,
    });
    if (!contributor) return next(new ErrorResponse('Forbidden', 403));
    delete req.body['isApproved'];
    delete req.body['isFeatured'];
  }

  let found = await Collection.findOne({
    _id: req.params.id,
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  if (!admin && found.contributor.toString() !== userId) {
    return next(new ErrorResponse('Forbidden', 403));
  }

  const updated = await Collection.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: updated,
  });
});

// @desc      Delete collection
// @route     DELETE /api/collection/:id
// @access    Private
exports.deleteCollection = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) {
    const contributor = Contributor.findOne({
      _id: ObjectId(userId),
      isApproved: true,
      isDisabled: false,
    });
    if (!contributor) return next(new ErrorResponse('Forbidden', 403));
  }

  let found = await Collection.findOne({
    _id: req.params.id,
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  if (!admin && found.contributor.toString() !== userId) {
    return next(new ErrorResponse('Forbidden', 403));
  }

  const updated = await Collection.findByIdAndUpdate(req.params.id, {
    isDisabled: true,
  });

  res.status(200).json({
    success: true,
  });
});
