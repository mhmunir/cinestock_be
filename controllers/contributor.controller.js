const { ObjectId } = require('mongodb');
const Admin = require('../models/Admin');
const Contributor = require('../models/Contributor');
const asyncHandler = require('../middlewares/async');
const ErrorResponse = require('../utils/errorResponse');
const multer = require('../utils/multer');

const uploadToS3 = multer.upload.single('file');

// @desc      Get all contributors
// @route     GET /api/contributor
// @access    Public
exports.getAllContributors = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc      Get pending contributors
// @route     GET /api/contributor/pending
// @access    Private
exports.getPendingContributors = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  res.status(200).json(res.advancedResults);
});

// @desc      Get single contributor
// @route     GET /api/contributor/:id
// @access    Public
exports.getContributor = asyncHandler(async (req, res, next) => {
  const contributor = await Contributor.findOne({
    _id: ObjectId(req.params.id),
    isApproved: true,
    isDisabled: false,
  })
    .populate({
      path: 'collections',
      select: '_id title description -contributor',
      match: { isApproved: true, isDisabled: false },
      populate: {
        path: 'videos',
        select: '_id title thumbnail preview -collections duration resolution',
        match: { isApproved: true, isDisabled: false },
        options: {
          limit: 3,
        },
      },
    })
    .populate({
      path: 'totalClips',
      match: { isApproved: true, isDisabled: false },
    });

  if (!contributor) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  res.status(200).json({
    success: true,
    data: contributor,
  });
});

// @desc      Upload contributor photo
// @route     PUT /api/v1/contributor/:id/upload
// @access    Private
exports.uploadPhoto = asyncHandler(async (req, res, next) => {
  uploadToS3(req, res, async (err) => {
    if (err || !req.file || !req.file.location) {
      return next(new ErrorResponse(err, 400));
    }

    const { userId } = req.user;
    const admin = await Admin.findById(userId);

    if (!admin) {
      const contributor = Contributor.findOne({
        _id: ObjectId(userId),
        isApproved: true,
        isDisabled: false,
      });
      if (!contributor) return next(new ErrorResponse('Forbidden', 403));
    }

    let found = await Contributor.findOne({
      _id: ObjectId(req.params.id),
      isDisabled: false,
    });

    if (!found) {
      return next(new ErrorResponse('Resource not found', 404));
    }

    const updated = await Contributor.findByIdAndUpdate(
      req.params.id,
      { photo: req.file.location },
      {
        new: true,
        runValidators: true,
      }
    );

    res.status(200).json({
      success: true,
      data: updated,
    });
  });
});

// @desc      Update contributor
// @route     PUT /api/v1/contributor/:id
// @access    Private
exports.updateContributor = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  let found = await Contributor.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  const updated = await Contributor.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: updated,
  });
});

// @desc      Delete contributor
// @route     DELETE /api/contributor/:id
// @access    Private
exports.deleteContributor = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  let found = await Contributor.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  const updated = await Contributor.findByIdAndUpdate(req.params.id, {
    isDisabled: true,
  });

  res.status(200).json({
    success: true,
  });
});
