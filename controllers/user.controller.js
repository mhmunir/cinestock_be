const { ObjectId } = require('mongodb');
const Admin = require('../models/Admin');
const User = require('../models/User');
const asyncHandler = require('../middlewares/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc      Get all users
// @route     GET /api/user
// @access    Private
exports.getAllUsers = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;

  const admin = Admin.findById(userId);
  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  res.status(200).json(res.advancedResults);
});

// @desc      Get single user
// @route     GET /api/user/:id
// @access    Private
exports.getUser = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;

  const admin = Admin.findById(userId);
  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  const found = await User.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  res.status(200).json({
    success: true,
    data: found,
  });
});

// @desc      Update user
// @route     PUT /api/v1/user/:id
// @access    Private
exports.updateUser = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  let found = await User.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  const updated = await User.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: updated,
  });
});

// @desc      Delete user
// @route     DELETE /api/user/:id
// @access    Private
exports.deleteUser = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) return next(new ErrorResponse('Forbidden', 403));

  let found = await User.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  const updated = await User.findByIdAndUpdate(req.params.id, {
    isDisabled: true,
  });

  res.status(200).json({
    success: true,
  });
});
