const { ObjectId } = require('mongodb');
const Admin = require('../models/Admin');
const Contributor = require('../models/Contributor');
const Video = require('../models/Video');
const VideoItem = require('../models/VideoItem');
const asyncHandler = require('../middlewares/async');
const ErrorResponse = require('../utils/errorResponse');
const multer = require('../utils/multer');

const uploadToS3 = multer.upload.single('file');

// @desc      Get all videos
// @route     GET /api/video
// @route     GET /api/contributor/:contributorId/videos
// @access    Public
exports.getAllVideos = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc      Get single video
// @route     GET /api/video/:id
// @access    Public
exports.getVideo = asyncHandler(async (req, res, next) => {
  const video = await Video.findOne({
    _id: ObjectId(req.params.id),
    isApproved: true,
    isDisabled: false,
  })
    .populate([
      {
        path: 'categories',
        select: 'title',
        match: { isDisabled: false },
      },
    ])
    .populate({
      path: 'collections',
      select: 'title description',
      match: { isApproved: true, isDisabled: false },
    })
    .populate({
      path: 'contributor',
      select: 'realName email',
      match: { isApproved: true, isDisabled: false },
    })
    .populate({
      path: 'videoItems',
      match: { isPreview: true, isDisabled: false },
    });

  if (!video) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  res.status(200).json({
    success: true,
    data: video,
  });
});

// @desc      Upload video
// @route     POST /api/video/upload
// @access    Private
exports.uploadVideo = (req, res, next) => {
  uploadToS3(req, res, async (err) => {
    if (err || !req.file || !req.file.location) {
      return next(
        new ErrorResponse('Error occured while uploading video', 400)
      );
    }

    try {
      const { userId } = req.user;
      const admin = await Admin.findById(userId);

      if (!admin) {
        const contributor = Contributor.findOne({
          _id: ObjectId(userId),
          isApproved: true,
          isDisabled: false,
        });
        if (!contributor) return next(new ErrorResponse('Forbidden', 403));
        delete req.body['isApproved'];
        delete req.body['isFeatured'];
      }

      const video = await Video.create({
        ...req.body,
        contributor: admin ? null : userId,
        byCinestock: !!admin,
      });

      const videoItem = await VideoItem.create({
        url: req.file.location,
        resolution: req.body.resolution,
        price: req.body.price,
        videoDetails: video._id,
      });

      res.status(201).json({
        success: true,
        data: video,
      });
    } catch (err) {
      next(err);
    }
  });
};

// @desc      Update video
// @route     PUT /api/v1/video/:id
// @access    Private
exports.updateVideo = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;

  const admin = await Admin.findById(userId);
  if (!admin) {
    const contributor = Contributor.findOne({
      _id: ObjectId(userId),
      isApproved: true,
      isDisabled: false,
    });
    if (!contributor) return next(new ErrorResponse('Forbidden', 403));
    delete req.body['isApproved'];
    delete req.body['isFeatured'];
  }

  let found = await Video.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  if (
    found.byCinestock
      ? !admin
      : !admin && found.contributor.toString() !== userId
  ) {
    return next(new ErrorResponse('Forbidden', 403));
  }

  const updated = await Video.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: updated,
  });
});

// @desc      Delete video
// @route     DELETE /api/video/:id
// @access    Private
exports.deleteVideo = asyncHandler(async (req, res, next) => {
  const { userId } = req.user;
  const admin = await Admin.findById(userId);

  if (!admin) {
    const contributor = Contributor.findOne({
      _id: ObjectId(userId),
      isApproved: true,
      isDisabled: false,
    });
    if (!contributor) return next(new ErrorResponse('Forbidden', 403));
  }

  let found = await Video.findOne({
    _id: ObjectId(req.params.id),
    isDisabled: false,
  });

  if (!found) {
    return next(new ErrorResponse('Resource not found', 404));
  }

  if (
    found.byCinestock
      ? !admin
      : !admin && found.contributor.toString() !== userId
  ) {
    return next(new ErrorResponse('Forbidden', 403));
  }

  const video = await Video.findByIdAndUpdate(req.params.id, {
    isDisabled: true,
  });

  res.status(200).json({
    success: true,
  });
});
