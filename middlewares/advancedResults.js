const advancedResults = (model, filters = {}, populate) => async (
  req,
  res,
  next
) => {
  let query;

  const reqQuery = { ...req.query };

  const excludedFields = ['search', 'select', 'sort', 'page', 'limit'];
  excludedFields.forEach((param) => delete reqQuery[param]);

  let queryStr = JSON.stringify(reqQuery);
  queryStr = queryStr.replace(
    /\b(gt|gte|lt|lte|in)\b/g,
    (match) => `$${match}`
  );

  if (req.params.searchKey) {
    const regex = new RegExp(req.params.searchKey, 'i');
    filters['title'] = regex;
  }
  if (req.params.categoryId) {
    filters['category'] = req.params.categoryId;
  }
  if (req.params.collectionId) {
    filters['collection'] = req.params.collectionId;
  }
  if (req.params.contributorId) {
    filters['contributor'] = req.params.contributorId;
  }

  query = model.find({
    ...JSON.parse(queryStr),
    ...filters,
    isDisabled: false,
  });

  if (populate) populate.forEach((p) => (query = query.populate(p)));

  if (req.query.select) {
    const fields = req.query.select.split(',').join(' ');
    query = query.select(fields);
  }

  if (req.query.sort) {
    const sortBy = req.query.sort.split(',').join(' ');
    query = query.sort(sortBy);
  } else {
    query = query.sort('-createdAt');
  }

  const count = await model.countDocuments({
    ...JSON.parse(queryStr),
    ...filters,
    isDisabled: false,
  });
  const page = parseInt(req.query.page, 10) || 1;
  const limit = parseInt(req.query.limit, 10) || 10;
  const skip = (page - 1) * limit;

  query = query.skip(skip).limit(limit);

  const results = await query;

  res.advancedResults = {
    success: true,
    totalItems: count,
    totalPages: Math.ceil(count / limit),
    currentPage: page,
    data: results,
  };

  next();
};

module.exports = advancedResults;
