const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    isDisabled: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

categorySchema.virtual('videos', {
  ref: 'Video',
  localField: '_id',
  foreignField: 'categories',
  justOne: false,
});

categorySchema.virtual('totalClips', {
  ref: 'Video',
  localField: '_id',
  foreignField: 'categories',
  justOne: false,
  count: true,
});

const Category = mongoose.model('Category', categorySchema);

module.exports = Category;
