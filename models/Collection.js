const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const collectionSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    contributor: {
      type: mongoose.Schema.ObjectId,
      ref: 'Contributor',
    },
    byCinestock: {
      type: Boolean,
      default: false,
    },
    thumbnail: {
      type: String,
      default: null,
    },
    isApproved: {
      type: Boolean,
      default: false,
    },
    isFeatured: {
      type: Boolean,
      default: false,
    },
    isDisabled: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

collectionSchema.virtual('videos', {
  ref: 'Video',
  localField: '_id',
  foreignField: 'collections',
  justOne: false,
});

collectionSchema.virtual('totalClips', {
  ref: 'Video',
  localField: '_id',
  foreignField: 'collections',
  justOne: false,
  count: true,
});

const Collection = mongoose.model('Collection', collectionSchema);

module.exports = Collection;
