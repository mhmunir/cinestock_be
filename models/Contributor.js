const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

const contributorSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
    },
    password: {
      type: String,
      required: true,
      select: false,
    },
    realName: {
      type: String,
    },
    photo: {
      type: String,
      default: null,
    },
    portfolioLinks: {
      type: [String],
    },
    socialLinks: {
      facebook: {
        type: String,
      },
      instagram: {
        type: String,
      },
      youtube: {
        type: String,
      },
      twitter: {
        type: String,
      },
    },
    isApproved: {
      type: Boolean,
      default: false,
    },
    isFeatured: {
      type: Boolean,
      default: false,
    },
    isDisabled: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

contributorSchema.virtual('collections', {
  ref: 'Collection',
  localField: '_id',
  foreignField: 'contributor',
  justOne: false,
});

contributorSchema.virtual('totalClips', {
  ref: 'Video',
  localField: '_id',
  foreignField: 'contributor',
  justOne: false,
  count: true,
});

contributorSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }

  this.password = await bcrypt.hash(this.password, 10);
});

contributorSchema.methods.matchPassword = async function (pass) {
  return await bcrypt.compare(pass, this.password);
};

const Contributor = mongoose.model('Contributor', contributorSchema);

module.exports = Contributor;
