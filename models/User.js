const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
require('dotenv').config();

const Schema = mongoose.Schema;

/**
 * - add isDisabled flag
 */

const userSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
    },
    password: {
      type: String,
      required: true,
      select: false,
    },
    favorites: {
      videos: {
        type: [String],
      },
      collections: {
        type: [String],
      },
      contributors: {
        type: [String],
      },
      select: false,
    },
    isDisabled: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }

  this.password = await bcrypt.hash(this.password, 10);
});

userSchema.methods.matchPassword = async function (pass) {
  return await bcrypt.compare(pass, this.password);
};

const User = mongoose.model('User', userSchema);

module.exports = User;
