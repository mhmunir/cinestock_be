const mongoose = require('mongoose');
const Collection = require('../models/Collection');

const Schema = mongoose.Schema;

const videoSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    url: {
      type: String,
      // required: true,
    },
    preview: {
      type: String,
    },
    thumbnail: {
      type: String,
    },
    collections: {
      type: Schema.Types.ObjectId,
      ref: 'Collection',
    },
    contributor: {
      type: Schema.Types.ObjectId,
      ref: 'Contributor',
    },
    byCinestock: {
      type: Boolean,
      default: false,
    },
    categories: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Category',
      },
    ],
    tags: {
      type: [String],
    },
    details: {
      type: String,
    },
    camera: {
      type: String,
    },
    duration: {
      type: Number,
    },
    resolution: {
      type: String,
    },
    aspectRatio: {
      type: String,
    },
    releaseTag: {
      type: String,
      enum: ['editorial', 'commercial'],
    },
    format: {
      type: String,
    },
    fps: {
      type: String,
    },
    framing: {
      type: String,
    },
    ethnicity: {
      type: String,
    },
    cameraMovement: {
      type: String,
    },
    gender: {
      type: String,
    },
    timeOfDay: {
      type: String,
    },
    age: {
      type: String,
    },
    numberOfPeople: {
      type: String,
    },
    location: {
      type: String,
    },
    speed: {
      type: String,
    },
    isProcessing: {
      type: Boolean,
      default: true,
    },
    isFeatured: {
      type: Boolean,
      default: false,
    },
    isApproved: {
      type: Boolean,
      default: false,
    },
    isDisabled: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

videoSchema.virtual('videoItems', {
  ref: 'VideoItem',
  localField: '_id',
  foreignField: 'videoDetails',
  justOne: false,
});

videoSchema.post('save', async function () {
  if (this.isModified('collections')) {
    await this.model('Collection').findByIdAndUpdate(this.collections, {
      thumbnail: this.thumbnail,
    });
  }
});

videoSchema.pre('findOneAndUpdate', async function (next) {
  if (!this._update.collections) {
    return next();
  }
  const video = await this.model.findById(this._conditions._id);
  await Collection.findByIdAndUpdate(this._update.collections, {
    thumbnail: video.thumbnail,
  });
});

const Video = mongoose.model('Video', videoSchema);

module.exports = Video;
