const mongoose = require('mongoose');
const axiosInstance = require('../utils/axios');

const Schema = mongoose.Schema;

const videoItemSchema = new Schema({
  url: {
    type: String,
    required: true,
  },
  resolution: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  currency: {
    type: String,
    default: '$',
  },
  videoDetails: {
    type: Schema.Types.ObjectId,
    ref: 'Video',
  },
  isDisabled: {
    type: Boolean,
    default: false,
  },
});

videoItemSchema.post('save', async function () {
  axiosInstance.post('processVideo', { id: this._id });
});

module.exports = mongoose.model('VideoItem', videoItemSchema);
