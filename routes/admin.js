const router = require('express').Router();
const { login } = require('../controllers/auth.controller');
const Admin = require('../models/Admin');

router.post('/login', login(Admin));

module.exports = router;
