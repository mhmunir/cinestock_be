const router = require('express').Router();
const {
  getAllCategories,
  getCategory,
  createCategory,
  updateCategory,
  deleteCategory,
} = require('../controllers/category.controller');
const { protect } = require('../middlewares/auth');
const advancedResults = require('../middlewares/advancedResults');
const Category = require('../models/Category');
const videoRouter = require('./video');

router.use('/:categoryId/videos', videoRouter);

router
  .route('/')
  .get(
    advancedResults(Category, {}, [
      {
        path: 'totalClips',
        match: { isApproved: true, isDisabled: false },
      },
    ]),
    getAllCategories
  )
  .post(protect, createCategory);

router
  .route('/:id')
  .get(getCategory)
  .put(protect, updateCategory)
  .delete(protect, deleteCategory);

module.exports = router;
