const router = require('express').Router();
const {
  getAllCollections,
  getCollection,
  createCollection,
  updateCollection,
  deleteCollection,
} = require('../controllers/collection.controller');
const { protect } = require('../middlewares/auth');
const advancedResults = require('../middlewares/advancedResults');
const Collection = require('../models/Collection');
const videoRouter = require('./video');

router.use('/:collectionId/videos', videoRouter);

router
  .route('/')
  .get(
    advancedResults(Collection, { isApproved: true }, [
      {
        path: 'contributor',
        select: '_id realName',
        match: { isApproved: true, isDisabled: false },
      },
      {
        path: 'totalClips',
        match: { isApproved: true, isDisabled: false },
      },
    ]),
    getAllCollections
  )
  .post(protect, createCollection);

router
  .route('/:id')
  .get(getCollection)
  .put(protect, updateCollection)
  .delete(protect, deleteCollection);

module.exports = router;
