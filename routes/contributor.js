const router = require('express').Router();
const {
  login,
  signup,
  getCurrentUser,
  sendResetLink,
  sendUsername,
  resetPassword,
} = require('../controllers/auth.controller');
const {
  getAllContributors,
  getPendingContributors,
  getContributor,
  uploadPhoto,
  updateContributor,
  deleteContributor,
} = require('../controllers/contributor.controller');
const { protect } = require('../middlewares/auth');
const advancedResults = require('../middlewares/advancedResults');
const Contributor = require('../models/Contributor');
const videoRouter = require('./video');

router.use('/:contributorId/videos', videoRouter);

router.get('/current', protect, getCurrentUser(Contributor));
router.get(
  '/pending',
  protect,
  advancedResults(Contributor, { isApproved: false }),
  getPendingContributors
);

router.post('/login', login(Contributor));
router.post('/signup', signup(Contributor));
router.post('/send-reset-link', sendResetLink(Contributor));
router.post('/send-username', sendUsername(Contributor));
router.post('/reset-password', resetPassword(Contributor));

router
  .route('/')
  .get(
    advancedResults(Contributor, { isApproved: true }, [
      { path: 'totalClips', match: { isApproved: true, isDisabled: false } },
    ]),
    getAllContributors
  );

router
  .route('/:id')
  .get(getContributor)
  .put(protect, updateContributor)
  .delete(protect, deleteContributor);

router.post('/:id/upload', protect, uploadPhoto);

module.exports = router;
