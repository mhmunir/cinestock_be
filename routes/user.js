const router = require('express').Router();
const {
  getCurrentUser,
  login,
  signup,
  sendResetLink,
  sendUsername,
  resetPassword,
} = require('../controllers/auth.controller');
const {
  getAllUsers,
  getUser,
  updateUser,
  deleteUser,
} = require('../controllers/user.controller');
const { protect } = require('../middlewares/auth');
const advancedResults = require('../middlewares/advancedResults');
const User = require('../models/User');

router.get('/current', protect, getCurrentUser(User));

router.post('/login', login(User));
router.post('/signup', signup(User));
router.post('/send-reset-link', sendResetLink(User));
router.post('/send-username', sendUsername(User));
router.post('/reset-password', resetPassword(User));

router.route('/').get(protect, advancedResults(User), getAllUsers);

router
  .route('/:id')
  .get(protect, getUser)
  .put(protect, updateUser)
  .delete(protect, deleteUser);

module.exports = router;
