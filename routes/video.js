const router = require('express').Router({ mergeParams: true });
const {
  getAllVideos,
  getVideo,
  uploadVideo,
  updateVideo,
  deleteVideo,
} = require('../controllers/video.controller');
const { protect } = require('../middlewares/auth');
const advancedResults = require('../middlewares/advancedResults');
const Video = require('../models/Video');
const VideoItem = require('../models/VideoItem');

router.post('/upload', protect, uploadVideo);
router.get(
  '/',
  advancedResults(Video, { isApproved: true }, [
    [{ path: 'categories', select: 'title', match: { isDisabled: false } }],
    {
      path: 'collections',
      select: 'title description',
      match: { isApproved: true, isDisabled: false },
    },
    {
      path: 'contributor',
      select: 'realName email',
      match: { isApproved: true, isDisabled: false },
    },
  ]),
  getAllVideos
);
router.get(
  '/search/:searchKey',
  advancedResults(Video, { isApproved: true }, [
    [{ path: 'categories', select: 'title', match: { isDisabled: false } }],
    {
      path: 'collections',
      select: '_id title description',
      match: { isApproved: true, isDisabled: false },
    },
    {
      path: 'contributor',
      select: '_id realName',
      match: { isApproved: true, isDisabled: false },
    },
  ]),
  getAllVideos
);

router
  .route('/:id')
  .get(getVideo)
  .put(protect, updateVideo)
  .delete(protect, deleteVideo);

module.exports = router;
