const fs = require('fs');
const mongoose = require('mongoose');

// Load env vars
require('dotenv').config();

// Load models
const Admin = require('./models/Admin');
const Category = require('./models/Category');
const Collection = require('./models/Collection');
const Contributor = require('./models/Contributor');
const User = require('./models/User');
const Video = require('./models/Video');
const VideoItem = require('./models/VideoItem');

// Connect to DB
mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

// Read JSON files
const admins = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/admins.json`, 'utf-8')
);
const categories = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/categories.json`, 'utf-8')
);
const collections = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/collections.json`, 'utf-8')
);
const contributors = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/contributors.json`, 'utf-8')
);
const users = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/users.json`, 'utf-8')
);
const videos = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/videos.json`, 'utf-8')
);
const videoItems = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/videoItems.json`, 'utf-8')
);

// Import into DB
const importData = async () => {
  try {
    await Admin.create(admins);
    await Category.create(categories);
    await Collection.create(collections);
    await Contributor.create(contributors);
    await User.create(users);
    await Video.create(videos);
    await VideoItem.create(videoItems);
    console.log('Data Imported...');
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

// Delete data
const deleteData = async () => {
  try {
    await Admin.deleteMany();
    await Category.deleteMany();
    await Collection.deleteMany();
    await Contributor.deleteMany();
    await User.deleteMany();
    await Video.deleteMany();
    await VideoItem.deleteMany();
    console.log('Data Destroyed...');
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

if (process.argv[2] === '-i') {
  importData();
} else if (process.argv[2] === '-d') {
  deleteData();
}
