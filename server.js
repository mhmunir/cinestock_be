// Importing modules
const app = require('express')();
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('./middlewares/error');
const mongoose = require('./utils/mongoose');

// Loading ENV vars
require('dotenv').config();

// Configuring Express app
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// MongoDB connection
mongoose.connect();

// Auth error handler
app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({ message: 'Unauthorized' });
  }
});

// Setting routes
app.use('/api/admin', require('./routes/admin'));
app.use('/api/category', require('./routes/category'));
app.use('/api/collection', require('./routes/collection'));
app.use('/api/contributor', require('./routes/contributor'));
app.use('/api/user', require('./routes/user'));
app.use('/api/video', require('./routes/video'));

// Error handler
app.use(errorHandler);

// Start server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`App running in ${process.env.MODE} mode on port ${PORT}`);
});
