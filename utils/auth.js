const jwtService = require('./jwt');

exports.getAuthToken = (req) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
};

exports.verifyAuthToken = (req) => {
  return new Promise((resolve, reject) => {
    const token = getAuthToken(req);
    if (token) {
      return jwtService.verifyToken(token).then((data) => {
        resolve(data);
      });
    } else {
      resolve(null);
    }
  });
};
