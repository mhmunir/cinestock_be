const axios = require('axios');
require('dotenv').config();

const axiosInstance = axios.create({
  baseURL: process.env.PROC_URL,
  headers: { 'api-key': process.env.PROC_API_KEY },
});

module.exports = axiosInstance;
