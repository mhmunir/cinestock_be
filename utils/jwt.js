const jwt = require('jsonwebtoken');
require('dotenv').config();

const defaultSecret = process.env.SECRET;
const defaultOptions = {
  expiresIn: '12h',
};

exports.createToken = (
  data,
  secret = defaultSecret,
  options = defaultOptions
) => {
  return new Promise((resolve, reject) => {
    return jwt.sign(data, secret, options, (err, token) => {
      if (err) reject(err);
      resolve(token);
    });
  });
};

exports.verifyToken = (
  token,
  secret = defaultSecret,
  options = defaultOptions
) => {
  return new Promise((resolve, reject) => {
    return jwt.verify(token, secret, options, (err, data) => {
      if (err) reject(err);
      resolve(data);
    });
  });
};
