const { ObjectId } = require('mongodb');

exports.getObjectId = (id) => {
  if (typeof id === 'object') {
    return id.map((item) => ObjectId(item));
  } else {
    return ObjectId(id);
  }
};
