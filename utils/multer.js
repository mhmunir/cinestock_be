const multer = require('multer');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');
require('dotenv').config();

const s3 = new aws.S3({
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_SECRET_KEY,
});

const getExtension = (path) => {
  let basename = path.split(/[\\/]/).pop(),
    pos = basename.lastIndexOf('.');
  if (basename === '' || pos < 1) return null;
  return basename.slice(pos + 1);
};

exports.upload = multer({
  storage: multerS3({
    s3: s3,
    // acl: 'public-read',
    bucket: process.env.S3_BUCKET,
    metadata: (req, file, cb) => {
      cb(null, { fieldName: file.fieldname });
    },
    key: (req, file, cb) => {
      cb(
        null,
        `${Date.now().toString()}-${uuidv4()}.${getExtension(
          file.originalname
        )}`
      );
    },
  }),
});
