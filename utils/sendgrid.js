const sgMail = require('@sendgrid/mail');
require('dotenv').config();

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

exports.sendEmail = (to, subject, html) => {
  return new Promise((resolve, reject) => {
    const msg = {
      to,
      from: 'cinestock@abduljalil.dev',
      subject,
      html,
    };
    return sgMail.send(msg).then(
      () => resolve(null),
      (error) => {
        reject(error);
      }
    );
  });
};
